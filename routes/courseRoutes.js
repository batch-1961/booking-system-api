

const express = require("express");
const router = express.Router();
const Course = require("../models/Course");

router.get('/',(req,res) =>{

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

});

router.post('/',(req,res)=>{

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price 

	})

	// console.log(newCourse);

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

});

module.exports = router;
