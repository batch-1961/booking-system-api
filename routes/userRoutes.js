

const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const User = require("../models/User");
const Detail = require("../models/User");



router.post('/',(req,res)=>{

	// console.log(req.body)
	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo


	})

	// console.log(newUser);

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

});

router.post('/details',(req,res)=>{

	let _id = req.body._id

	User.find({_id})
	.then(result => res.send(result))
	.catch(error => res.send(error))

	})

module.exports = router;
