const express = require("express");
const mongoose = require("mongoose");
const app = express();
app.use(express.json())
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin123@cluster0.cnvebhq.mongodb.net/bookingAPI?retryWrites=true&w=majority",

{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB Connection Error."))

db.once('open', () => console.log("Connected to MongoDB."))

const courseRoutes = require('./routes/courseRoutes');
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes); 
app.use('/details',userRoutes);

 


app.listen(port,() => console.log(`Express API running at port 4000`))